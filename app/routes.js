Router.configure({
  // we use the  appBody template to define the layout for the entire app
  layoutTemplate: 'main',

  // the appNotFound template is used for unknown routes and missing lists
  //notFoundTemplate: 'notFound',

  // wait on the following subscriptions before rendering the page to ensure
  // the data it's expecting is present
  waitOn: function () {
    return [];
  },
  onBeforeAction: function () {
    Session.set('modalClass', this.route.getName());
    this.next();
  },
  onAfterAction: function () {
    //handle modal
    if (this.route.getName() !== 'home') {
      Meteor.setTimeout(function () {
        //init tab
        $('.tabular.menu .item').tab();
        //show modal
        $('.ui.modal').modal('show');
      }, 1);
    }
  }
});

Router.route('home', {
  path: '/'
});

Router.route('profile', {
  path: '/profile'
});

Router.route('showDraw', {
  path: '/draw/:id',
  data: function () {
    var draw = Draws.findOne({id: parseInt(this.params.id)});
    if (draw && draw.endDate) {
      // reset time
      draw.endDate.setHours(0, 0, 0, 0);
    }
    Session.set('draw', draw);
    return draw;
  },
  action: function () {
    this.render();
  }
});

Router.route('page', {
  path: '/page/:url',
  data: function () {
    return Pages.findOne({url: this.params.url})
  },
  action: function () {
    this.render();
  }
});

Router.route('login');

Router.route('register');
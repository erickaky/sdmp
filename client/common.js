getCountDown = function (args) {
  var days,
    hours,
    minutes,
    seconds,
    formats = {
      'd': function () {
        return days;
      },
      'day': function () {
        var d = days.toString();
        var lastChar = d.substr(-1, 1);

        if ((days < 10 || days > 20)) {
          if (lastChar == '1') {
            return 'день';
          }
          if (['2', '3', '4'].indexOf(lastChar) >= 0) {
            return 'дня';
          }
        }
        return 'дней';
      },
      'h': function () {
        return hours;
      },
      'm': function () {
        return minutes;
      },
      's': function () {
        return seconds;
      }
    },
    getLeadingZero = function (value) {
      return (value < 10) ? '0' + value : value;
    },
    format = function (format) {
      var result;

      result = format.replace(/%(\w+)/g, function (match, id) {
        return typeof formats[id] == 'function' ? formats[id]() : match;
      });

      return result;
    },
    getComponent = function (int, base) {
      var component;

      if (base == '24') {
        component = 23 - int;
      } else {
        component = 59 - int;
      }

      if (args.leadingZero) {
        component = getLeadingZero(component);
      }

      return component;
    };

  if (!args.endDate) {
    throw new Error('Please specify "endDate" param');
  }

  if (!args.startDate) {
    args.startDate = new Date();
  }

  days = Math.floor(Math.round(args.endDate - args.startDate) / 86400000);
  hours = getComponent(args.startDate.getHours(), 24);
  minutes = getComponent(args.startDate.getMinutes());
  seconds = getComponent(args.startDate.getSeconds());

  if (args.dateFormat) {
    return format(args.dateFormat);
  }

  return {days: days, hours: hours, minutes: minutes, seconds: seconds};
};

hideModal = function () {
  $('.ui.modal').modal('hide');
};

toggleScroll = function (status) {
  /**
   * left: 37, up: 38, right: 39, down: 40
   * spaceBar: 32, pageUp: 33, pageDown: 34, end: 35, home: 36
   */
  var keys = {37: 1, 38: 1, 39: 1, 40: 1};

  function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault) {
      e.preventDefault();
    }
    e.returnValue = false;
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  }

  status = !!status;

  if (status) {
    if (window.removeEventListener) {
      window.removeEventListener('DOMMouseScroll', preventDefault, false);
    }
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
  } else {
    if (window.addEventListener) {
      window.addEventListener('DOMMouseScroll', preventDefault, false);
    }
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    document.onkeydown  = preventDefaultForScrollKeys;
  }
};
getScrollbarWidth = function () {
  var outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.width = "100px";
  outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

  document.body.appendChild(outer);

  var widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = "scroll";

  // add innerdiv
  var inner = document.createElement("div");
  inner.style.width = "100%";
  outer.appendChild(inner);

  var widthWithScroll = inner.offsetWidth;

  // remove divs
  outer.parentNode.removeChild(outer);

  return widthNoScroll - widthWithScroll;
};
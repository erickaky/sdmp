Template.login.events({
  'submit form': function (e) {
    var login = $('[name=login]').val();
    var password = $('[name=password]').val();

    Meteor.loginWithPassword(login, password, function (error) {
      if (error) {
        var message = 'Не правильный логин или пароль';

        if (error.reason == 'User not found') {
          message = 'Пользователь не существует';
        }

        this.$('form').form('add prompt', 'login');
        this.$('.error input')
          .popup({
            inline: true,
            content: message
          })
        ;
      } else {
        hideModal();
      }
    });
    e.preventDefault();
  }
});

Template.login.rendered = function () {
  this.$('form').form({
    fields: {
      login: {
        identifier: 'login',
        rules: [{type: 'empty'}]
      },
      password: {
        identifier: 'password',
        rules: [{type: 'empty'}]
      }
    }
  });
};
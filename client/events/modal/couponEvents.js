Template.coupon.events({
  'click .ui.button': function (e) {
    var user = Meteor.user();
    var draw = Template.parentData();
    var soldCouponsCount = 0;
    var message;

    if (user) {
      soldCouponsCount = Coupons.find({
        status: 'buyed',
        userId: user._id,
        drawId: draw._id
      }).count();
      if (draw.status != 'active') {
        message = 'Розыгрыш уже завершен';
      } else if (user.balance >= draw.couponPrice && draw.couponCountPerOne > soldCouponsCount) {
        Meteor.call('buyCoupon', this);
      } else {
        message = 'Недостаточно средств. Пожалуйста, пополните баланс';

        if (draw.couponCountPerOne <= soldCouponsCount) {
          message = 'Вы приобрели максимальное количество билетиков'
        }
      }
      if (message) {
        $(e.target).popup({
          content: message,
          position: 'bottom left'
        }).trigger('mouseover');
      }
    }
  }
});

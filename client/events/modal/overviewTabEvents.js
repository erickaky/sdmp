Template.overviewTab.rendered = function () {
  if (!Meteor.userId()) {
    Meteor.setTimeout(function () {
      $('.coupon.card .ui.button').popup({
        content: 'Пожалуйста войдите в систему',
        position: 'bottom left'
      })
    }, 500);
  }
};


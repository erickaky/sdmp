Template.modal.onRendered(function () {
  this.$('.ui.modal').modal({
    onHidden: function () {
      Router.go('home');
      document.body.classList.remove('rendered');
    },
    onVisible: function () {
      document.body.classList.add('rendered')
    }
  })
});
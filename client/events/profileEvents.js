Template.profile.events({
  'click #user-password-submit': function (e) {
    var passwords = $('input[type="password"]');

    Accounts.changePassword(passwords[0].value, passwords[1].value, function (error) {
      if (!error) {
        e.target.textContent = 'Сохраненно';
      } else {
        shake($(passwords[0]));
      }
    });
  },
  'click #user-details-submit': function (e) {
    var name = $('input[name="name"]')[0],
      secondName = $('input[name="secondName"]')[0],
      phone = $('input[name="phone"]')[0];

    if (name.value && secondName.value && phone.value) {
      Meteor.call('updateUser', name.value, secondName.value, phone.value);
    } else {
      if (!name.value) {
        shake(name);
      }
      if (!secondName.value) {
        shake(secondName);
      }
      if (!phone.value) {
        shake(phone);
      }
    }
  }
});

function shake(element) {
  $(element).transition({
    animation: 'shake'
  });
}


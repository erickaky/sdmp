Template.register.events({
  'submit form': function (e) {
    var login = $('[name=login]').val();
    var email = $('[name=email]').val();
    var password = $('[name=password]').val();

    Accounts.createUser({
      username: login,
      email: email,
      password: password
    }, function (error) {
      if (error) {
        var message;

        switch (error.reason) {
          case 'Email already exists.':
          {
            this.$('form').form('add prompt', 'email');
            message = 'E-mail уже используется.';
            break;
          }
          case 'Username already exists.':
          {
            this.$('form').form('add prompt', 'login');
            message = 'Пользователь уже зарегистрирован.';
            break;
          }
        }

        this.$('.error input')
          .popup({
            inline: true,
            content: message
          })
        ;
      } else {
        hideModal();
        Meteor.call('sendVerificationLink');
      }
    });

    e.preventDefault()
  }
});

Template.register.rendered = function () {
  this.$('form').form({
    fields: {
      login: {
        identifier: 'login',
        rules: [{type: 'empty'}]
      },
      email: {
        identifier: 'email',
        rules: [{type: 'email'}]
      },
      password: {
        identifier: 'password',
        rules: [{type: 'empty'}]
      },
      eula: {
        identifier: 'eula',
        rules: [{type: 'checked'}]
      }
    }
  });
};

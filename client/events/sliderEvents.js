Template.slider.onRendered(function () {
  var options = {
      $AutoPlay: true,
      $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slideshow is auto playing, default value is false

      $ArrowKeyNavigation: false,   			            //Allows arrow key to navigate or not
      $SlideWidth: 600,                                   //[Optional] Width of every slide in pixels, the default is width of 'slides' container
      //$SlideHeight: 280,                                  //[Optional] Height of every slide in pixels, the default is width of 'slides' container
      $SlideSpacing: 0, 					                //Space between each slide in pixels
      $DisplayPieces: 2,                                  //Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
      $ParkingPosition: 100,                                //The offset position to park slide (this options applys only when slideshow disabled).

      $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
        $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
        $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
        $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
      }
    },
    mainSlider,
    selector = 'slider';

    mainSlider = new $JssorSlider$(selector, options);
    ScaleSlider();

  function ScaleSlider() {
    var parentWidth = mainSlider.$Elmt.parentNode.clientWidth;
    if (parentWidth) {
      mainSlider.$ScaleWidth(Math.min(parentWidth, 900));
    } else {
      window.setTimeout(ScaleSlider, 30);
    }
  }
});

Template.coupon.helpers({
  isSelf: function () {
    return this.userId == Meteor.userId();
  },
  username: function () {
    return ReactiveMethod.call('getUsername', this.userId);
  }
});

Template.draw.helpers({
  buyCount: function () {
    return Coupons.find({drawId: this._id, status: {$ne: null}, userId: {$exists: true, $ne: ''}}).count();
  },
  endDateFormatted: function () {
    if (this.status == 'cancelled') {
      return 'все средства возвращены';
    } else if (this.status == 'played') {
      return 'розыгрыш завершен';
    } else if (this.endDate.setHours(0, 0, 0, 0) && this.endDate <= Session.get('time')) {
      return 'розыгрывается...'
    } else {
      return 'до розыгрыша: ' +
        getCountDown({
          endDate: this.endDate,
          leadingZero: true,
          dateFormat: '%d %day, %h:%m:%s',
          startDate: Session.get('time')
        });
    }
  },
  coverImage: function () {
    if (Session.get('imagesReady')) {
      var img = Images.findOne({_id: this.cover}),
        result = {
          key: 'no-photo.svg'
        };

      if (img) {
        result = img.copies.original;
      }

      return result;
    }
  }
});

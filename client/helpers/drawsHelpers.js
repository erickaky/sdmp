Template.draws.helpers({
  draws: function () {
    var status = this.status || 'active';
    var query = {
      status: status
    };

    if (status == 'past') {
      query.status = {
        $ne: 'active'
      }
    }

    if (status == 'active') {
      query.endDate = {
        $gte: Session.get('time')
      };
    }
    return Draws.find(query);
  },
  columns: function () {
    if (window.matchMedia('(max-width: 736px)').matches) {
      return 'one'
    } else if (window.matchMedia('(max-width: 1024px)').matches) {
      return 'two';
    } else {
      return 'four';
    }
  }
});

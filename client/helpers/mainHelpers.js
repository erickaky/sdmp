Template.main.helpers({
  imagesReady: function() {
    return Session.get('imagesReady');
  },
  anySlides: function() {
    return Sliders.find().count() > 0;
  },
  phrase: function() {
    var list = [
      'Протираем стол',
      'Тусуем билеты',
      'Оплачиваем налоги',
      'Выпрямляем карты',
      'Входим в азарт',
      'Ждем бармена',
      'Прячемся от жены',
      'Срываем куш',
      'Достатаем заначку'
    ];
    var index = Math.floor(Math.random() * list.length);

    return list[index];
  }
});
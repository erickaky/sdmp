Template.menu.helpers({
  currentUserLogin: function () {
    var username;
    var user = Meteor.user();

    if (user.username) {
      username = user.username;
      Meteor.setTimeout(function () {
        $('.ui.dropdown').dropdown();
      }, 1);
    }

    return username;
  }
});
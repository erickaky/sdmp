Template.couponsTab.helpers({
  coupons: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return [];
    }

    return Coupons.find({'drawId': draw._id});
  },
  n: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return 0;
    }

    return draw.couponCountPerOne;
  },
  columns: function () {
    if (window.matchMedia('(max-width: 736px)').matches) {
      return 'two'
    } else {
      return 'four';
    }
  }
});

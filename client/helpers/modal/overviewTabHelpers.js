Template.overviewTab.helpers({
  draw: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return [];
    }

    return draw;
  },
  images: function () {
    var draw = Session.get('draw'),
      imagesReady = Session.get('imagesReady'),
      imagesCursor,
      images = [];

    if (draw && imagesReady) {
      if (Array.isArray(draw.image) && draw.image[0]) {
        imagesCursor = Images.find({_id: {$in: draw.image}});
        if (imagesCursor) {
          imagesCursor.forEach(function (image) {
            images.push(image.copies.original);
          });
        }
      } else {
        images.push({key: 'no-photo.svg'});
      }

      return images;
    }
  }
});

Template.partnerTab.helpers({
  partner: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return {};
    }

    return Partners.findOne(draw.partnerId);
  },
  image: function () {
    var draw = Session.get('draw'),
      imagesReady = Session.get('imagesReady'),
      partner,
      result = {
        key: 'no-photo.svg'
      },
      img;

    if (draw && imagesReady) {
      partner = Partners.findOne(draw.partnerId);
      if (partner) {
        img = Images.findOne({_id: partner.image});
        if (img) {
          result = img.copies.original;
        }

        return result;
      }
    }
  }
});

Template.profileDrawTab.helpers({
  draws: function () {
    var user = Meteor.user();
    var draws = [];

    if (user) {
      var drawCollection = {};
      var coupons = Coupons.find({userId: user._id}).fetch();

      for (var coupon in coupons) {
        if (coupons.hasOwnProperty(coupon)) {
          var drawId = coupons[coupon]['drawId'];
          if (!drawCollection[drawId]) {
            drawCollection[drawId] = [];
          }
          drawCollection[drawId].push(coupons[coupon]);
        }
      }
      for (var draw in drawCollection) {
        if (drawCollection.hasOwnProperty(draw)) {
          drawCollection[draw][drawCollection[draw].length - 1].isLast = true;
          draws.push({
            draw: Draws.findOne(draw),
            coupons: drawCollection[draw]
          })
        }
      }
    }

    return draws;
  },
  endDateFormatted: function () {
    if (this.draw.status == 'cancelled') {
      return 'все средства возвращены <br /><span class="red">розыгрыш анулирован</span>';
    } else if (this.draw.status == 'played') {
      var coupon = Coupons.findOne({drawId: this.draw._id, isWinning: true});
      var winner = coupon.winnerName || '';
      return '<span class="green">розыгрыш состоялся <br />счастливчик ' + winner +'</span>';
    } else if (this.draw.endDate.setHours(0, 0, 0, 0) && this.draw.endDate <= Session.get('time')) {
      return 'розыгрывается...'
    } else {
      return 'до розыгрыша: ' +
        getCountDown({
          endDate: this.draw.endDate,
          leadingZero: true,
          dateFormat: '%d %day, %h:%m:%s',
          startDate: Session.get('time')
        });
    }
  }
});

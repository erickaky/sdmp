Template.profileTab.helpers({
  currentUserEmail: function () {
    var email;
    var user = Meteor.user();

    if (user && user.emails && user.emails.length) {
      email = user.emails[0].address;
      Meteor.setTimeout(function () {
        $('.ui.dropdown').dropdown();
      }, 1);
    }

    return email;
  }
});

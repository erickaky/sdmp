Template.profile.helpers({
  balance: function () {
    var user = Meteor.user();
    var balance = 0;

    if (user && user.balance) {
      balance = user.balance
    }

    return balance;
  },
  mobile: function () {
    return window.matchMedia('(max-width: 736px)').matches;
  }
});

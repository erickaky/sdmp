Template.showDraw.helpers({
  endDateFormatted: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return {};
    }

    return getCountDown({
      endDate: draw.endDate,
      leadingZero: true,
      dateFormat: '%d %day, %h:%m:%s',
      startDate: Session.get('time')
    });
  },
  isActive: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return false;
    }

    return draw.status == 'active';
  },
  isCancelled: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return false;
    }

    return draw.status == 'cancelled';
  },
  isPlayedOut: function () {
    var draw = Session.get('draw');

    if (!draw) {
      return false;
    }

    return draw.status == 'active' && (draw.endDate <= Session.get('time'));
  },
  mobile: function () {
    return window.matchMedia('(max-width: 736px)').matches;
  }
});

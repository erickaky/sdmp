Template.slider.helpers({
  slides: function () {
    var slides = Sliders.find({}),
      imagesReady = Session.get('imagesReady'),
      images = [];

    if (slides && imagesReady) {
      slides.forEach(function (slide) {
        var image = Images.findOne(slide.image);

        if (image) {
          // inject url and push images
          image.copies.original.url = slide.url;
          images.push(image.copies.original);
        }
      });
    }

    if (images.length) {
      return images;
    }
  }
});

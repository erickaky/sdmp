Meteor.startup(function () {
  setInterval(function () {
    Meteor.call('getServerTime', function (error, result) {
      Session.set('time', result);
    });
  }, 1000);

  // set images loaded flag
  Session.set('imagesReady', false);
});

Tracker.autorun(function () {
  Meteor.subscribe('userData');
});

Meteor.subscribe('draws');
Meteor.subscribe('coupons');
Meteor.subscribe('sliders');
Meteor.subscribe('partners');
Meteor.subscribe('pages');
Meteor.subscribe('images', function () {
  Session.set('imagesReady', true);
});

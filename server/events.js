Accounts.onCreateUser(function (options, user) {
  var userId = 100;
  var lastUser = Meteor.users.findOne({}, {sort: {id: -1}});

  if (lastUser && lastUser.id) {
    userId = lastUser.id + 1;
  }

  user.id = userId;
  user.balance = 0;

  return user;
});
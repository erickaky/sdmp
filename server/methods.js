Meteor.methods({
  getServerTime: function () {
    return new Date();
  },
  buyCoupon: function (coupon) {
    var user = Meteor.user();
    var draw = Draws.findOne(coupon.drawId);
    var soldCouponsCount;

    if (!user || !coupon) {
      return;
    }

    soldCouponsCount = Coupons.find({
      status: 'buyed',
      userId: user._id,
      drawId: draw._id
    }).count();

    if (user.balance >= draw.couponPrice && draw.couponCountPerOne >= soldCouponsCount) {
      Coupons.update(coupon._id, {
        $set: {
          userId: user._id,
          status: 'buyed'
        }
      });
      Meteor.users.update(user._id, {$inc: {balance: -parseInt(draw.couponPrice)}});
    }
  },
  getUsername: function (userId) {
    var user = Meteor.users.findOne(userId);
    var result = 'anonymous';

    if (user) {
      result = user.username;
    }

    return result;
  },
  updateUser: function (name, secondName, phone) {
    var user = Meteor.user();

    // we can update user details only once
    if (!user || user.name || user.secondName || user.phone) {
      return;
    }

    Meteor.users.update({_id: user._id}, {$set: {
      name: name,
      secondName: secondName,
      phone: phone
    }});
  },
  sendVerificationLink: function () {
    var user = Meteor.user();

    if (!user || !user.emails || user.emails[0].verified) {
      return;
    }

    return Accounts.sendVerificationEmail(user._id);
  }
});

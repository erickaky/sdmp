Meteor.publish('userData', function () {
  return Meteor.users.find({_id: this.userId}, {fields: {id: 1, balance: 1, name: 1, secondName: 1, phone: 1}});
});
Meteor.publish('draws', function () {
  return Draws.find();
});
Meteor.publish('coupons', function () {
  return Coupons.find();
});
Meteor.publish('sliders', function () {
  return Sliders.find();
});
Meteor.publish('partners', function () {
  return Partners.find();
});
Meteor.publish('pages', function () {
  return Pages.find();
});
Meteor.publish('images', function () {
  return Images.find();
});